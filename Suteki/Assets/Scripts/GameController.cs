﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject _menuCanvas, _gameOverCanvas;
    [SerializeField] private PlayerController _player;
    [SerializeField] private LevelsController _levelsController;
    [SerializeField] private CameraFollow _cameraFollow;
    [SerializeField] private float _reloadTimer;

    private void OnValidate()
    {
        _player = FindObjectOfType<PlayerController>();
        _levelsController = FindObjectOfType<LevelsController>();
        _cameraFollow = FindObjectOfType<CameraFollow>();
    }

    private void Start()
    {
        _gameOverCanvas.SetActive(false);
        _menuCanvas.SetActive(true);
        _player.Stop();
        _levelsController.Init();
        _cameraFollow.Stop();
    }

    public void StartGame()
    {
        _menuCanvas.SetActive(false);
        _levelsController.StartLevel();
    }

    public void NextLevel()
    {
        _cameraFollow.Stop();
        _player.Stop();
        _levelsController.StartLevel();
    }

    public void GameOver()
    {
        _player.gameObject.SetActive(false);
        _gameOverCanvas.SetActive(true);
        StartCoroutine(ReloadScene());
    }

    private IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(_reloadTimer);
        Reload();
    }

    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}