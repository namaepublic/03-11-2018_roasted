using System.Collections.Generic;
using UnityEngine;

public static class Helpers
{
    public static List<T> GetComponentsInChildrenWithTag<T>(this Component component, string tag, bool recursive = false)
        where T : Component
    {
        var list = new List<T>();
        foreach (var item in component.GetComponentsInChildren<T>(recursive))
            if (item.CompareTag(tag))
                list.Add(item);
        return list;
    }

    public static T GetComponentInChildrenWithTag<T>(this Component component, string tag, bool includeInactive = false)
        where T : Component
    {
        var item = component.GetComponentInChildren<T>(includeInactive);
        return item != null && item.CompareTag(tag) ? item : null;
    }
}