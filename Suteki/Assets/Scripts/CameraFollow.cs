﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{
    [SerializeField] [HideInInspector] private Camera _camera;
    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _initialSize, _targetedSize;
    [SerializeField] [Range(0, 30)] private float _smoothSpeed;

    private bool _init;

    private void OnValidate()
    {
        _camera = GetComponent<Camera>();
    }

    public void Init()
    {
        _init = true;
    }

    public void Stop()
    {
        _init = false;
        _camera.orthographicSize = _initialSize;
    }

    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, _target.position + _offset, _smoothSpeed * Time.deltaTime);
        if (_init)
            _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, _targetedSize, _smoothSpeed * Time.deltaTime);
    }
}