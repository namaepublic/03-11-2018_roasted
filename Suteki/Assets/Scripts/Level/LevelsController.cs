﻿using UnityEngine;

public class LevelsController : MonoBehaviour
{
	[SerializeField] private LevelController[] _levels;
	[SerializeField] private GameController _gameController;

	public int Level { get; set; }
	
	private void OnValidate()
	{
		_levels = GetComponentsInChildren<LevelController>(true);
		_gameController = FindObjectOfType<GameController>();
	}

	public void Init()
	{
		Level = 0;
	}

	public void StartLevel()
	{
		if (Level >= _levels.Length)
			_gameController.Reload();
		
		for (var i = 0; i < _levels.Length; i++)
			_levels[i].Init(i == Level);
		Level++;
	}
}
