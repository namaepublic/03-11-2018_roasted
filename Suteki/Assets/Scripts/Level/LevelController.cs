﻿using System.Collections;
using TMPro;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private PlayerController _player;
    [SerializeField] private Transform _start;
    [SerializeField] private float _spawnCooldown = 3;
    [SerializeField] private TextMeshProUGUI _timerText;
    [SerializeField] private GameObject _guiCanvas;
    [SerializeField] private CameraFollow _cameraFollow;

    private void OnValidate()
    {
        _player = FindObjectOfType<PlayerController>();
        _start = this.GetComponentsInChildrenWithTag<Transform>("Start")[0];
        _cameraFollow = FindObjectOfType<CameraFollow>();
    }

    public void Init(bool value)
    {
        gameObject.SetActive(value);
        if (value)
            StartCoroutine(PlayerSpawn());
    }

    private IEnumerator PlayerSpawn()
    {
        _player.SetPosition(_start.position);
        _guiCanvas.SetActive(true);
        for (var i = 0; i < _spawnCooldown; i++)
        {
            _timerText.text = (_spawnCooldown - i).ToString();
            yield return new WaitForSeconds(1);
        }

        _guiCanvas.SetActive(false);
        _player.Init();
        _cameraFollow.Init();
    }
}