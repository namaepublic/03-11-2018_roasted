﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] [HideInInspector] private GameController _gameController;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;
    [SerializeField] [HideInInspector] private Rigidbody2D _rigidbody;
    [SerializeField] [HideInInspector] private int _fryPanLayer, _arrivalLayer;

    [SerializeField] private int _maxHp;
    [SerializeField] [HideInInspector] private int _topHp, _bottomHp;
    [SerializeField] private int _damage;
    [SerializeField] private float _damageCooldown;
    [SerializeField] private float _damageMultiplier;

    [SerializeField] private Image _topBarImage, _bottomBarImage;

    private bool _wantToJump;
    private bool _canJump, _canTakeDamage;
    private bool _upsideDown;

    private float _currentTime;

    private bool _topBarEmpty, _bottomBarEmpty;
    private bool _multiplyDmg;
    private bool _init;

    public int TopHp
    {
        get { return _topHp; }
        set
        {
            if (value > _maxHp)
                _topHp = _maxHp;
            else if (value <= 0)
            {
                _topBarEmpty = true;
                _topHp = 0;
                LoseBar();
            }
            else
                _topHp = value;

            _topBarImage.fillAmount = (float) _topHp / _maxHp;
        }
    }

    public int BottomHp
    {
        get { return _bottomHp; }
        set
        {
            if (value > _maxHp)
                _bottomHp = _maxHp;
            else if (value <= 0)
            {
                _bottomBarEmpty = true;
                _bottomHp = 0;
                LoseBar();
            }
            else
                _bottomHp = value;

            _bottomBarImage.fillAmount = (float) _bottomHp / _maxHp;
        }
    }

    private void OnValidate()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _fryPanLayer = LayerMask.NameToLayer("FryPan");
        _arrivalLayer = LayerMask.NameToLayer("Finish");
        _gameController = FindObjectOfType<GameController>();
    }

    private void OnMouseDown()
    {
        if (!_init)
            _gameController.StartGame();
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }
    
    public void Init()
    {
        _init = true;
        _rigidbody.isKinematic = false;
    }

    public void Stop()
    {
        _rigidbody.velocity = Vector2.zero;
        _rigidbody.isKinematic = true;
        _init = false;
        _topBarEmpty = _bottomBarEmpty = false;
        TopHp = BottomHp = _maxHp;
        _multiplyDmg = false;
    }

    private void Update()
    {
        if (!_init) return;

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            transform.position += Vector3.right * _speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
            transform.position += Vector3.left * _speed * Time.deltaTime;

        _wantToJump = Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow);

        if (Input.GetKeyDown(KeyCode.Space))
            Rotate();

        if (!_canTakeDamage) return;

        if (_currentTime >= _damageCooldown)
            TakeDamage();
        else
            _currentTime += Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (_wantToJump)
            Jump();
    }

    private void Jump()
    {
        if (_canJump)
            _rigidbody.AddForce(Vector3.up * _jumpForce);
    }

    private void Rotate()
    {
        transform.Rotate(Vector3.forward, 180);
        _upsideDown = !_upsideDown;
    }


    private void TakeDamage()
    {
        _currentTime = 0;

        if (!_multiplyDmg)
        {
            if (_upsideDown)
                TopHp -= _damage;
            else
                BottomHp -= _damage;
        }
        else
        {
            TopHp -= (int) (_damage * _damageMultiplier);
            BottomHp -= (int) (_damage * _damageMultiplier);
        }
    }

    private void LoseBar()
    {
        if (_bottomBarEmpty && _topBarEmpty)
            _gameController.GameOver();
        else
            _multiplyDmg = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == _fryPanLayer)
            _currentTime = _damageCooldown;
        if (other.gameObject.layer == _arrivalLayer)
            _gameController.NextLevel();
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        _canJump = true;
        _canTakeDamage = other.gameObject.layer == _fryPanLayer;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _canJump = false;
        _canTakeDamage = false;
    }
}